﻿Imports MySql.Data.MySqlClient
Public Class menu
    Dim conn As New koneksi
    Private Sub KeluarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeluarToolStripMenuItem.Click
        Me.Dispose()
    End Sub

    Private Sub menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = FormWindowState.Maximized
        conn.open()
        conn.close()
    End Sub

    Private Sub EntriDataToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntriDataToolStripMenuItem.Click
        EntriDataAnggota.ShowDialog()
    End Sub

    Private Sub EntriDataSimpananToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        EntriDataSimpanan.ShowDialog()
    End Sub

    Private Sub EntriPinjamanToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntriPinjamanToolStripMenuItem1.Click
        EntriPinjaman.ShowDialog()
    End Sub

    Private Sub EntriAngsuranToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntriAngsuranToolStripMenuItem.Click
        EntriAngsuran.ShowDialog()
    End Sub

    Private Sub CetakSurveyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CetakSurveyToolStripMenuItem.Click
        CetakSurvey.ShowDialog()
    End Sub

    Private Sub CetakSPPDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CetakSPPDToolStripMenuItem.Click
        CetakSPPD.ShowDialog()
    End Sub

    Private Sub EntriPengunduranToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntriPengunduranToolStripMenuItem.Click
        EntriPengunduranDiri.ShowDialog()
    End Sub

    Private Sub EntriPinjamanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntriPinjamanToolStripMenuItem.Click
        EntriPendaftaran.ShowDialog()
    End Sub

    Private Sub EntriSimpananToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntriSimpananToolStripMenuItem.Click
        EntriDataSimpanan.ShowDialog()
    End Sub
End Class
