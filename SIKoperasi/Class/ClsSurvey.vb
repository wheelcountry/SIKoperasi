﻿Imports MySql.Data.MySqlClient
Public Class ClsSurvey
    Public no_survey As String
    Public tgl_survey As Date
    Public tanggungan As String
    Public penghasilan As Integer
    Public pengeluaran As Integer
    Public lama_usaha As String
    Public simpanan_lancar As Integer
    Public simpanan_kelompok As Integer
    Public jaminan_lain As String
    Public kesimpulan As String
    Public no_pinjaman As String
    Public Sub Simpan()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        cmd.CommandText = "insert into survey (no_survey, tgl_survey, tanggungan, penghasilan, pengeluaran, lama_usaha, simpanan_lancar, simpanan_kelompok, jaminan_lain, kesimpulan, no_pinjaman)" & _
        "values('" & no_survey & "','" & Format(Me.tgl_survey, "yyyy-MM-dd") & "','" & tanggungan & "','" & penghasilan & "','" & pengeluaran & "','" & lama_usaha & "','" & simpanan_lancar & "','" & simpanan_kelompok & "','" & jaminan_lain & "','" & kesimpulan & "','" & no_pinjaman & "')"
        cmd.Connection = conn.open
        Dim x As Integer
        x = cmd.ExecuteNonQuery
        If x = 1 Then
            MessageBox.Show("Data tersimpan.", "Pesan")
        Else
            MessageBox.Show("wat")
        End If
        cmd.Connection.Close()
    End Sub

    Public Function Tampil_Survey()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        Dim sql As String = ""
        Dim dreader As MySqlDataReader
        Dim tmpreader As New List(Of ClsSurvey)
        cmd.CommandText = "select * from survey order by no_survey asc"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.HasRows Then
            While dreader.Read
                Dim objTemp As New ClsSurvey
                objTemp.no_survey = dreader.Item("no_survey")
                objTemp.tgl_survey = dreader.Item("tgl_survey")
                tmpreader.Add(objTemp)
            End While
        End If
        dreader.Close()
        Return tmpreader
    End Function

    Public Sub Cetak()

    End Sub

    Public Function Autonumber()
        Dim conn As New koneksi
        Dim dreader As MySqlDataReader
        Dim cmd As New MySqlCommand
        Dim strTemp As String = ""
        Dim konter As String = ""
        Dim kode As String

        cmd.CommandText = "select count(*) as nomor from survey"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.Read = True Then
            strTemp = dreader.Item("nomor")
            konter = Val(strTemp) + 1
            kode = ("SV" & Mid("00000", 1, 5 - konter.Length) & konter)
        Else
            kode = "SV0001"
            Return (kode)
            Exit Function
        End If
        cmd.Connection.Close()
        Return (kode)
    End Function
End Class
