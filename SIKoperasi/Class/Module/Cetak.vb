﻿Module Cetak

    'Cetak Transaksi
    Sub CtkSurvey(ByVal no_survey As String)
        Dim myreport As New CRCetakSurvey

        myreport.SetParameterValue("parameter", no_survey)
        Dim f As New FrmTempCr
        f.CrystalReportViewer1.ReportSource = myreport
        f.CrystalReportViewer1.Zoom(100)

        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    'Sub Ctkpengeluaran(ByVal no_suratpesanan As String)
    '    Dim myreport As New CryPengeluaran

    '    myreport.SetParameterValue("parameter", no_suratpesanan)
    '    Dim f As New FrmTempCr
    '    f.CrystalReportViewer1.ReportSource = myreport
    '    f.CrystalReportViewer1.Zoom(100)

    '    f.WindowState = FormWindowState.Maximized
    '    f.Show()
    'End Sub

    'Sub CtkMemo(ByVal no_memo As String)
    '    Dim myreport As New CrPenyesuaianBarang

    '    myreport.SetParameterValue("parameter", no_memo)
    '    Dim f As New FrmTempCr
    '    f.CrystalReportViewer1.ReportSource = myreport
    '    f.CrystalReportViewer1.Zoom(100)

    '    f.WindowState = FormWindowState.Maximized
    '    f.Show()
    'End Sub

    'Cetak Laporan
    Sub CtkLapPendaftaran(ByVal Tglawal As Date, ByVal Tglakhir As Date)
        Dim myreport As New CRLapPendaftaran

        myreport.SetParameterValue("tgl1", Tglawal)
        myreport.SetParameterValue("tgl2", Tglakhir)

        Dim myTemp As New FrmTempCr
        myTemp.CrystalReportViewer1.ReportSource = myreport
        myTemp.CrystalReportViewer1.ShowGroupTreeButton = False
        myTemp.CrystalReportViewer1.Zoom(100)
        myTemp.WindowState = FormWindowState.Maximized
        myTemp.Show()
    End Sub

    Sub CtkLapSimpanan(ByVal Tglawal As Date, ByVal Tglakhir As Date)
        Dim myreport As New CRLapSimpanan

        myreport.SetParameterValue("tgl1", Tglawal)
        myreport.SetParameterValue("tgl2", Tglakhir)
        Dim f As New FrmTempCr
        f.CrystalReportViewer1.ReportSource = myreport
        f.CrystalReportViewer1.Zoom(100)

        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    Sub CtkLapPinjaman(ByVal Tglawal As Date, ByVal Tglakhir As Date)
        Dim myreport As New CRLapPinjaman

        myreport.SetParameterValue("tgl1", Tglawal)
        myreport.SetParameterValue("tgl2", Tglakhir)
        Dim f As New FrmTempCr
        f.CrystalReportViewer1.ReportSource = myreport
        f.CrystalReportViewer1.Zoom(100)

        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub

    Sub CtkLapAngsuran(ByVal Tglawal As Date, ByVal Tglakhir As Date)
        Dim myreport As New CRLapAngsuran

        myreport.SetParameterValue("tgl1", Tglawal)
        myreport.SetParameterValue("tgl2", Tglakhir)
        Dim f As New FrmTempCr
        f.CrystalReportViewer1.ReportSource = myreport
        f.CrystalReportViewer1.Zoom(100)

        f.WindowState = FormWindowState.Maximized
        f.Show()
    End Sub
End Module
