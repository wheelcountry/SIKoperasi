﻿Imports MySql.Data.MySqlClient
Public Class ClsPendaftaran

    Public no_pendaftaran As String
    Public tgl_pendaftaran As Date
    Public bidang_usaha As String
    Public penjualan_perbulan As String
    Public keuntungan_perbulan As String
    Public besar_modal As String
    Public tempat_tinggal As String
    Public tempat_usaha As String
    Public almt_usaha As String
    Public no_anggota As String

    Public Function Autonumber()
        Dim conn As New koneksi
        Dim dreader As MySqlDataReader
        Dim cmd As New MySqlCommand
        Dim strTemp As String = ""
        Dim konter As String = ""
        Dim kode As String

        cmd.CommandText = "select count(*) as nomor from pendaftaran"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.Read = True Then
            strTemp = dreader.Item("nomor")
            konter = Val(strTemp) + 1
            kode = ("PD" & Mid("00000", 1, 5 - konter.Length) & konter)
        Else
            kode = "PD0001"
            Return (kode)
            Exit Function
        End If
        cmd.Connection.Close()
        Return (kode)
    End Function

    Public Sub Simpan()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        cmd.CommandText = "insert into pendaftaran (no_pendaftaran, tgl_pendaftaran, bidang_usaha, penjualan_perbulan, keuntungan_perbulan, besar_modal, tempat_tinggal, tempat_usaha, almt_usaha, no_anggota)" & _
        "values('" & no_pendaftaran & "','" & Format(Me.tgl_pendaftaran, "yyyy-MM-dd") & "','" & bidang_usaha & "','" & penjualan_perbulan & "','" & keuntungan_perbulan & "','" & besar_modal & "','" & tempat_tinggal & "','" & tempat_usaha & "','" & almt_usaha & "','" & no_anggota & "')"
        cmd.Connection = conn.open
        Dim x As Integer
        x = cmd.ExecuteNonQuery
        If x = 1 Then
            MessageBox.Show("Data tersimpan.", "Pesan")
        Else
            MessageBox.Show("wat")
        End If
        cmd.Connection.Close()
    End Sub

    Public Sub Get_NoPendaftaran(ByVal kode As String)
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        Dim dreader As MySqlDataReader
        cmd.CommandText = "select * from pendaftaran where no_anggota='" & kode & "'"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        dreader.Read()
        If dreader.HasRows Then
            no_anggota = dreader("no_anggota").ToString
            no_pendaftaran = dreader("no_pendaftaran").ToString
            bidang_usaha = dreader("bidang_usaha").ToString
            penjualan_perbulan = dreader("penjualan_perbulan").ToString
            keuntungan_perbulan = dreader("keuntungan_perbulan").ToString
            besar_modal = dreader("besar_modal").ToString
            tempat_tinggal = dreader("tempat_tinggal").ToString
            tempat_usaha = dreader("tempat_usaha").ToString
            almt_usaha = dreader("almt_usaha").ToString
        Else
            no_anggota = ""
            no_pendaftaran = ""
            bidang_usaha = ""
            penjualan_perbulan = ""
            keuntungan_perbulan = ""
            besar_modal = ""
            tempat_tinggal = ""
            tempat_usaha = ""
            almt_usaha = ""
        End If
    End Sub
End Class
