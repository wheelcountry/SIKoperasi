﻿Imports MySql.Data.MySqlClient
Public Class ClsPinjaman
    Public no_pinjaman As String
    Public tgl_pinjaman As Date
    Public jml_pinjaman As Integer
    Public keterangan As String
    Public lama_angsur As String
    Public no_anggota As String

    Public Function Autonumber()
        Dim conn As New koneksi
        Dim dreader As MySqlDataReader
        Dim cmd As New MySqlCommand
        Dim strTemp As String = ""
        Dim konter As String = ""
        Dim kode As String

        cmd.CommandText = "select count(*) as nomor from pinjaman"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.Read = True Then
            strTemp = dreader.Item("nomor")
            konter = Val(strTemp) + 1
            kode = ("PI" & Mid("00000", 1, 5 - konter.Length) & konter)
        Else
            kode = "PI0001"
            Return (kode)
            Exit Function
        End If
        cmd.Connection.Close()
        Return (kode)
    End Function

    Public Sub Simpan()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        cmd.CommandText = "insert into pinjaman (no_pinjaman, tgl_pinjaman, jml_pinjaman, keterangan, lama_angsur, no_anggota)" & _
        "Values('" & no_pinjaman & "','" & Format(Me.tgl_pinjaman, "yyyy-MM-dd") & "','" & jml_pinjaman & "','" & keterangan & "','" & lama_angsur & "','" & no_anggota & "')"
        cmd.Connection = conn.open
        Dim x As Integer
        x = cmd.ExecuteNonQuery
        If x = 1 Then
            MessageBox.Show("Data tersimpan.", "Pesan")
        Else
            MessageBox.Show("wat")
        End If
        cmd.Connection.Close()
    End Sub

    Public Function Tampil_Pinjaman()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        Dim sql As String = ""
        Dim dreader As MySqlDataReader
        Dim tmpreader As New List(Of ClsPinjaman)

        'cmd.CommandText = "select a.no_ttb, a.tgl_ttb, a.no_ref_sj, b.no_po, c.kd_supplier, c.nm_supplier from ttb a, po b, supplier c where a.no_po=b.no_po and b.kd_Supplier=c.kd_supplier and a.no_ttb not in (SELECT no_ttb from retur) "
        'cmd.CommandText = cmd.CommandText & "group by a.no_ttb order by no_ttb asc"
        'cmd.CommandText = "select a.no_pinjaman, a.jml_pinjaman, b.nm_anggota, c.bidang_usaha from pinjaman a, anggota b, pendaftaran c where a.no_anggota=b.no_anggota and b.no_anggota=c.no_anggota order by a.no_pinjaman asc"
        cmd.CommandText = "select no_pinjaman, tgl_pinjaman, jml_pinjaman, no_anggota from pinjaman order by no_pinjaman asc"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.HasRows Then
            While dreader.Read
                Dim objTemp As New ClsPinjaman
                objTemp.no_pinjaman = dreader.Item("no_pinjaman")
                objTemp.tgl_pinjaman = dreader.Item("tgl_pinjaman")
                objTemp.jml_pinjaman = dreader.Item("jml_pinjaman")
                objTemp.no_anggota = dreader.Item("no_anggota")
                tmpreader.Add(objTemp)
            End While
        End If
        dreader.Close()
        Return tmpreader
    End Function
End Class
