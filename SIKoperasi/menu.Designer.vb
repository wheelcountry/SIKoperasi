﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class menu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EntriDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TransaksiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EntriPinjamanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EntriSimpananToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EntriPinjamanToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.CetakSurveyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CetakSPPDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EntriAngsuranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EntriPengunduranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanPendaftaranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanSimpananToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanPinjamanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanAngsuranToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LaporanPengunduranDiriToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MasterToolStripMenuItem, Me.TransaksiToolStripMenuItem, Me.LaporanToolStripMenuItem, Me.KeluarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(634, 25)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntriDataToolStripMenuItem})
        Me.MasterToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(61, 21)
        Me.MasterToolStripMenuItem.Text = "Master"
        '
        'EntriDataToolStripMenuItem
        '
        Me.EntriDataToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EntriDataToolStripMenuItem.Name = "EntriDataToolStripMenuItem"
        Me.EntriDataToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.EntriDataToolStripMenuItem.Text = "Entri Data Anggota"
        '
        'TransaksiToolStripMenuItem
        '
        Me.TransaksiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntriPinjamanToolStripMenuItem, Me.EntriSimpananToolStripMenuItem, Me.EntriPinjamanToolStripMenuItem1, Me.CetakSurveyToolStripMenuItem, Me.CetakSPPDToolStripMenuItem, Me.EntriAngsuranToolStripMenuItem, Me.EntriPengunduranToolStripMenuItem})
        Me.TransaksiToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TransaksiToolStripMenuItem.Name = "TransaksiToolStripMenuItem"
        Me.TransaksiToolStripMenuItem.Size = New System.Drawing.Size(74, 21)
        Me.TransaksiToolStripMenuItem.Text = "Transaksi"
        '
        'EntriPinjamanToolStripMenuItem
        '
        Me.EntriPinjamanToolStripMenuItem.Name = "EntriPinjamanToolStripMenuItem"
        Me.EntriPinjamanToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.EntriPinjamanToolStripMenuItem.Text = "Entri Pendaftaran"
        '
        'EntriSimpananToolStripMenuItem
        '
        Me.EntriSimpananToolStripMenuItem.Name = "EntriSimpananToolStripMenuItem"
        Me.EntriSimpananToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.EntriSimpananToolStripMenuItem.Text = "Entri Simpanan"
        '
        'EntriPinjamanToolStripMenuItem1
        '
        Me.EntriPinjamanToolStripMenuItem1.Name = "EntriPinjamanToolStripMenuItem1"
        Me.EntriPinjamanToolStripMenuItem1.Size = New System.Drawing.Size(207, 22)
        Me.EntriPinjamanToolStripMenuItem1.Text = "Entri Pinjaman"
        '
        'CetakSurveyToolStripMenuItem
        '
        Me.CetakSurveyToolStripMenuItem.Name = "CetakSurveyToolStripMenuItem"
        Me.CetakSurveyToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.CetakSurveyToolStripMenuItem.Text = "Cetak Survey"
        '
        'CetakSPPDToolStripMenuItem
        '
        Me.CetakSPPDToolStripMenuItem.Name = "CetakSPPDToolStripMenuItem"
        Me.CetakSPPDToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.CetakSPPDToolStripMenuItem.Text = "Cetak SPPD"
        '
        'EntriAngsuranToolStripMenuItem
        '
        Me.EntriAngsuranToolStripMenuItem.Name = "EntriAngsuranToolStripMenuItem"
        Me.EntriAngsuranToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.EntriAngsuranToolStripMenuItem.Text = "Entri Angsuran"
        '
        'EntriPengunduranToolStripMenuItem
        '
        Me.EntriPengunduranToolStripMenuItem.Name = "EntriPengunduranToolStripMenuItem"
        Me.EntriPengunduranToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.EntriPengunduranToolStripMenuItem.Text = "Entri Pengunduran Diri"
        '
        'LaporanToolStripMenuItem
        '
        Me.LaporanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LaporanPendaftaranToolStripMenuItem, Me.LaporanSimpananToolStripMenuItem, Me.LaporanPinjamanToolStripMenuItem, Me.LaporanAngsuranToolStripMenuItem, Me.LaporanPengunduranDiriToolStripMenuItem})
        Me.LaporanToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LaporanToolStripMenuItem.Name = "LaporanToolStripMenuItem"
        Me.LaporanToolStripMenuItem.Size = New System.Drawing.Size(68, 21)
        Me.LaporanToolStripMenuItem.Text = "Laporan"
        '
        'LaporanPendaftaranToolStripMenuItem
        '
        Me.LaporanPendaftaranToolStripMenuItem.Name = "LaporanPendaftaranToolStripMenuItem"
        Me.LaporanPendaftaranToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.LaporanPendaftaranToolStripMenuItem.Text = "Laporan Pendaftaran"
        '
        'LaporanSimpananToolStripMenuItem
        '
        Me.LaporanSimpananToolStripMenuItem.Name = "LaporanSimpananToolStripMenuItem"
        Me.LaporanSimpananToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.LaporanSimpananToolStripMenuItem.Text = "Laporan Simpanan"
        '
        'LaporanPinjamanToolStripMenuItem
        '
        Me.LaporanPinjamanToolStripMenuItem.Name = "LaporanPinjamanToolStripMenuItem"
        Me.LaporanPinjamanToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.LaporanPinjamanToolStripMenuItem.Text = "Laporan Pinjaman"
        '
        'LaporanAngsuranToolStripMenuItem
        '
        Me.LaporanAngsuranToolStripMenuItem.Name = "LaporanAngsuranToolStripMenuItem"
        Me.LaporanAngsuranToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.LaporanAngsuranToolStripMenuItem.Text = "Laporan Angsuran"
        '
        'LaporanPengunduranDiriToolStripMenuItem
        '
        Me.LaporanPengunduranDiriToolStripMenuItem.Name = "LaporanPengunduranDiriToolStripMenuItem"
        Me.LaporanPengunduranDiriToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.LaporanPengunduranDiriToolStripMenuItem.Text = "Laporan Pengunduran Diri"
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(57, 21)
        Me.KeluarToolStripMenuItem.Text = "Keluar"
        '
        'menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(634, 262)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "menu"
        Me.Text = "Menu Utama Sistem Informasi Koperasi"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransaksiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KeluarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntriDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntriPinjamanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntriPinjamanToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntriAngsuranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSurveyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CetakSPPDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntriPengunduranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntriSimpananToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPendaftaranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanSimpananToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPinjamanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanAngsuranToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LaporanPengunduranDiriToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
