﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CetakSPPD
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dt_sppd = New System.Windows.Forms.DateTimePicker
        Me.txt_total_penyaluran = New System.Windows.Forms.TextBox
        Me.txt_nilai_penyaluran = New System.Windows.Forms.TextBox
        Me.txt_keputusan = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txt_administrasi = New System.Windows.Forms.TextBox
        Me.txt_bagi_hasil = New System.Windows.Forms.TextBox
        Me.txt_jangka_waktu = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txt_kegunaan = New System.Windows.Forms.TextBox
        Me.txt_plafond = New System.Windows.Forms.TextBox
        Me.txt_no_sppd = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btn_kuar = New System.Windows.Forms.Button
        Me.btn_batal = New System.Windows.Forms.Button
        Me.btn_simpan = New System.Windows.Forms.Button
        Me.txt_no_survey = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dt_survey = New System.Windows.Forms.DateTimePicker
        Me.btn_cari_simpanan = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dt_sppd)
        Me.GroupBox1.Controls.Add(Me.txt_total_penyaluran)
        Me.GroupBox1.Controls.Add(Me.txt_nilai_penyaluran)
        Me.GroupBox1.Controls.Add(Me.txt_keputusan)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txt_administrasi)
        Me.GroupBox1.Controls.Add(Me.txt_bagi_hasil)
        Me.GroupBox1.Controls.Add(Me.txt_jangka_waktu)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txt_kegunaan)
        Me.GroupBox1.Controls.Add(Me.txt_plafond)
        Me.GroupBox1.Controls.Add(Me.txt_no_sppd)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 185)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(435, 341)
        Me.GroupBox1.TabIndex = 32
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data SPPD"
        '
        'dt_sppd
        '
        Me.dt_sppd.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_sppd.CustomFormat = "dd/MM/yyyy"
        Me.dt_sppd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_sppd.Location = New System.Drawing.Point(144, 65)
        Me.dt_sppd.Name = "dt_sppd"
        Me.dt_sppd.Size = New System.Drawing.Size(200, 20)
        Me.dt_sppd.TabIndex = 30
        '
        'txt_total_penyaluran
        '
        Me.txt_total_penyaluran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_total_penyaluran.Location = New System.Drawing.Point(144, 231)
        Me.txt_total_penyaluran.MaxLength = 9
        Me.txt_total_penyaluran.Name = "txt_total_penyaluran"
        Me.txt_total_penyaluran.Size = New System.Drawing.Size(179, 22)
        Me.txt_total_penyaluran.TabIndex = 10
        '
        'txt_nilai_penyaluran
        '
        Me.txt_nilai_penyaluran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nilai_penyaluran.Location = New System.Drawing.Point(144, 259)
        Me.txt_nilai_penyaluran.MaxLength = 9
        Me.txt_nilai_penyaluran.Name = "txt_nilai_penyaluran"
        Me.txt_nilai_penyaluran.Size = New System.Drawing.Size(179, 22)
        Me.txt_nilai_penyaluran.TabIndex = 11
        '
        'txt_keputusan
        '
        Me.txt_keputusan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_keputusan.Location = New System.Drawing.Point(144, 287)
        Me.txt_keputusan.MaxLength = 45
        Me.txt_keputusan.Name = "txt_keputusan"
        Me.txt_keputusan.Size = New System.Drawing.Size(179, 22)
        Me.txt_keputusan.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 290)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 16)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Keputusan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 234)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(110, 16)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Total Penyaluran"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 262)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(106, 16)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Nilai Penyaluran"
        '
        'txt_administrasi
        '
        Me.txt_administrasi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_administrasi.Location = New System.Drawing.Point(144, 203)
        Me.txt_administrasi.MaxLength = 9
        Me.txt_administrasi.Name = "txt_administrasi"
        Me.txt_administrasi.Size = New System.Drawing.Size(179, 22)
        Me.txt_administrasi.TabIndex = 9
        '
        'txt_bagi_hasil
        '
        Me.txt_bagi_hasil.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_bagi_hasil.Location = New System.Drawing.Point(144, 175)
        Me.txt_bagi_hasil.MaxLength = 9
        Me.txt_bagi_hasil.Name = "txt_bagi_hasil"
        Me.txt_bagi_hasil.Size = New System.Drawing.Size(179, 22)
        Me.txt_bagi_hasil.TabIndex = 8
        '
        'txt_jangka_waktu
        '
        Me.txt_jangka_waktu.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jangka_waktu.Location = New System.Drawing.Point(144, 147)
        Me.txt_jangka_waktu.MaxLength = 2
        Me.txt_jangka_waktu.Name = "txt_jangka_waktu"
        Me.txt_jangka_waktu.Size = New System.Drawing.Size(179, 22)
        Me.txt_jangka_waktu.TabIndex = 7
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 206)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 16)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Administrasi"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(13, 178)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 16)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Bagi Hasil"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(13, 150)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 16)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Jangka Waktu"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(13, 122)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Kegunaan"
        '
        'txt_kegunaan
        '
        Me.txt_kegunaan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_kegunaan.Location = New System.Drawing.Point(144, 119)
        Me.txt_kegunaan.MaxLength = 45
        Me.txt_kegunaan.Name = "txt_kegunaan"
        Me.txt_kegunaan.Size = New System.Drawing.Size(179, 22)
        Me.txt_kegunaan.TabIndex = 30
        '
        'txt_plafond
        '
        Me.txt_plafond.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_plafond.Location = New System.Drawing.Point(144, 91)
        Me.txt_plafond.MaxLength = 30
        Me.txt_plafond.Name = "txt_plafond"
        Me.txt_plafond.Size = New System.Drawing.Size(241, 22)
        Me.txt_plafond.TabIndex = 9
        '
        'txt_no_sppd
        '
        Me.txt_no_sppd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_sppd.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_sppd.Name = "txt_no_sppd"
        Me.txt_no_sppd.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_sppd.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Plafond"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tgl. SPPD"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. SPPD"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 38)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(74, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "No. Survey"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(13, 66)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(76, 16)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Tgl. Survey"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_kuar)
        Me.GroupBox3.Controls.Add(Me.btn_batal)
        Me.GroupBox3.Controls.Add(Me.btn_simpan)
        Me.GroupBox3.Location = New System.Drawing.Point(23, 589)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(435, 75)
        Me.GroupBox3.TabIndex = 35
        Me.GroupBox3.TabStop = False
        '
        'btn_kuar
        '
        Me.btn_kuar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_kuar.Location = New System.Drawing.Point(310, 26)
        Me.btn_kuar.Name = "btn_kuar"
        Me.btn_kuar.Size = New System.Drawing.Size(75, 32)
        Me.btn_kuar.TabIndex = 15
        Me.btn_kuar.Text = "Keluar"
        Me.btn_kuar.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Location = New System.Drawing.Point(179, 26)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(78, 32)
        Me.btn_batal.TabIndex = 14
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan
        '
        Me.btn_simpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan.Location = New System.Drawing.Point(31, 26)
        Me.btn_simpan.Name = "btn_simpan"
        Me.btn_simpan.Size = New System.Drawing.Size(90, 32)
        Me.btn_simpan.TabIndex = 13
        Me.btn_simpan.Text = "Simpan"
        Me.btn_simpan.UseVisualStyleBackColor = True
        '
        'txt_no_survey
        '
        Me.txt_no_survey.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_survey.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_survey.Name = "txt_no_survey"
        Me.txt_no_survey.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_survey.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(33, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(167, 31)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Cetak SPPD"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dt_survey)
        Me.GroupBox2.Controls.Add(Me.btn_cari_simpanan)
        Me.GroupBox2.Controls.Add(Me.txt_no_survey)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Location = New System.Drawing.Point(23, 82)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(435, 97)
        Me.GroupBox2.TabIndex = 34
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Survey"
        '
        'dt_survey
        '
        Me.dt_survey.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_survey.CustomFormat = "dd/MM/yyyy"
        Me.dt_survey.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_survey.Location = New System.Drawing.Point(144, 66)
        Me.dt_survey.Name = "dt_survey"
        Me.dt_survey.Size = New System.Drawing.Size(200, 20)
        Me.dt_survey.TabIndex = 31
        '
        'btn_cari_simpanan
        '
        Me.btn_cari_simpanan.Location = New System.Drawing.Point(329, 35)
        Me.btn_cari_simpanan.Name = "btn_cari_simpanan"
        Me.btn_cari_simpanan.Size = New System.Drawing.Size(75, 23)
        Me.btn_cari_simpanan.TabIndex = 0
        Me.btn_cari_simpanan.Text = "Cari"
        Me.btn_cari_simpanan.UseVisualStyleBackColor = True
        '
        'CetakSPPD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(485, 680)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CetakSPPD"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cetak SPPD"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dt_sppd As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_total_penyaluran As System.Windows.Forms.TextBox
    Friend WithEvents txt_nilai_penyaluran As System.Windows.Forms.TextBox
    Friend WithEvents txt_keputusan As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_administrasi As System.Windows.Forms.TextBox
    Friend WithEvents txt_bagi_hasil As System.Windows.Forms.TextBox
    Friend WithEvents txt_jangka_waktu As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_kegunaan As System.Windows.Forms.TextBox
    Friend WithEvents txt_plafond As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_sppd As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_kuar As System.Windows.Forms.Button
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents btn_simpan As System.Windows.Forms.Button
    Friend WithEvents txt_no_survey As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_cari_simpanan As System.Windows.Forms.Button
    Friend WithEvents dt_survey As System.Windows.Forms.DateTimePicker
End Class
