﻿Imports MySql.Data.MySqlClient
Public Class CetakSPPD
    Dim sppd As New clssppd
    Private Sub CetakSPPD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Bersih()
    End Sub
    Private Sub Bersih()
        txt_no_sppd.Enabled = False
        txt_no_sppd.Text = sppd.Autonumber
        txt_no_survey.Enabled = False
        txt_no_sppd.Enabled = False
        dt_sppd.Value = Now
        txt_plafond.Clear()
        txt_kegunaan.Clear()
        txt_jangka_waktu.Clear()
        txt_bagi_hasil.Clear()
        txt_administrasi.Clear()
        txt_total_penyaluran.Clear()
        txt_nilai_penyaluran.Clear()
        txt_keputusan.Clear()
        btn_cari_simpanan.Focus()
    End Sub
    Private Sub SetData()
        sppd.no_survey = txt_no_survey.Text
        sppd.no_sppd = txt_no_sppd.Text
        sppd.tgl_sppd = dt_sppd.Value
        sppd.plafond = txt_plafond.Text
        sppd.kegunaan = txt_kegunaan.Text
        sppd.jangka_waktu = txt_jangka_waktu.Text
        sppd.bagi_hasil = txt_bagi_hasil.Text
        sppd.administrasi = txt_administrasi.Text
        sppd.total_penyaluran = txt_total_penyaluran.Text
        sppd.nilai_penyaluran = txt_nilai_penyaluran.Text
        sppd.keputusan = txt_keputusan.Text
    End Sub
    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        sppd.Simpan()
        Bersih()
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Bersih()
    End Sub

    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub

    Private Sub btn_cari_simpanan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari_simpanan.Click
        popupSurvey.ShowDialog()
    End Sub
End Class