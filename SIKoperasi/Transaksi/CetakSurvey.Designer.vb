﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CetakSurvey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dt_survey = New System.Windows.Forms.DateTimePicker
        Me.txt_simp_kel = New System.Windows.Forms.TextBox
        Me.txt_jaminan = New System.Windows.Forms.TextBox
        Me.txt_kesimpulan = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txt_simp_lancar = New System.Windows.Forms.TextBox
        Me.txt_lama = New System.Windows.Forms.TextBox
        Me.txt_pengeluaran = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txt_penghasilan = New System.Windows.Forms.TextBox
        Me.txt_tanggungan = New System.Windows.Forms.TextBox
        Me.txt_no_survey = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txt_bidang = New System.Windows.Forms.TextBox
        Me.asdafasd = New System.Windows.Forms.Label
        Me.btn_cari_simpanan = New System.Windows.Forms.Button
        Me.txt_jml = New System.Windows.Forms.TextBox
        Me.txt_nama = New System.Windows.Forms.TextBox
        Me.txt_no_pinj = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btn_kuar = New System.Windows.Forms.Button
        Me.btn_batal = New System.Windows.Forms.Button
        Me.btn_simpan = New System.Windows.Forms.Button
        Me.txt_no_anggota_tmp = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dt_survey)
        Me.GroupBox1.Controls.Add(Me.txt_simp_kel)
        Me.GroupBox1.Controls.Add(Me.txt_jaminan)
        Me.GroupBox1.Controls.Add(Me.txt_kesimpulan)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txt_simp_lancar)
        Me.GroupBox1.Controls.Add(Me.txt_lama)
        Me.GroupBox1.Controls.Add(Me.txt_pengeluaran)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txt_penghasilan)
        Me.GroupBox1.Controls.Add(Me.txt_tanggungan)
        Me.GroupBox1.Controls.Add(Me.txt_no_survey)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 243)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(435, 341)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Survey"
        '
        'dt_survey
        '
        Me.dt_survey.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_survey.CustomFormat = "dd/MM/yyyy"
        Me.dt_survey.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_survey.Location = New System.Drawing.Point(144, 65)
        Me.dt_survey.Name = "dt_survey"
        Me.dt_survey.Size = New System.Drawing.Size(200, 20)
        Me.dt_survey.TabIndex = 30
        '
        'txt_simp_kel
        '
        Me.txt_simp_kel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_simp_kel.Location = New System.Drawing.Point(144, 231)
        Me.txt_simp_kel.MaxLength = 9
        Me.txt_simp_kel.Name = "txt_simp_kel"
        Me.txt_simp_kel.Size = New System.Drawing.Size(179, 22)
        Me.txt_simp_kel.TabIndex = 10
        '
        'txt_jaminan
        '
        Me.txt_jaminan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jaminan.Location = New System.Drawing.Point(144, 259)
        Me.txt_jaminan.MaxLength = 30
        Me.txt_jaminan.Name = "txt_jaminan"
        Me.txt_jaminan.Size = New System.Drawing.Size(179, 22)
        Me.txt_jaminan.TabIndex = 11
        '
        'txt_kesimpulan
        '
        Me.txt_kesimpulan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_kesimpulan.Location = New System.Drawing.Point(144, 287)
        Me.txt_kesimpulan.MaxLength = 45
        Me.txt_kesimpulan.Name = "txt_kesimpulan"
        Me.txt_kesimpulan.Size = New System.Drawing.Size(179, 22)
        Me.txt_kesimpulan.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 290)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 16)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Kesimpulan"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 234)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(133, 16)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Simpanan Kelompok"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 262)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 16)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Jaminan Lain"
        '
        'txt_simp_lancar
        '
        Me.txt_simp_lancar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_simp_lancar.Location = New System.Drawing.Point(144, 203)
        Me.txt_simp_lancar.MaxLength = 9
        Me.txt_simp_lancar.Name = "txt_simp_lancar"
        Me.txt_simp_lancar.Size = New System.Drawing.Size(179, 22)
        Me.txt_simp_lancar.TabIndex = 9
        '
        'txt_lama
        '
        Me.txt_lama.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_lama.Location = New System.Drawing.Point(144, 175)
        Me.txt_lama.MaxLength = 2
        Me.txt_lama.Name = "txt_lama"
        Me.txt_lama.Size = New System.Drawing.Size(179, 22)
        Me.txt_lama.TabIndex = 8
        '
        'txt_pengeluaran
        '
        Me.txt_pengeluaran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pengeluaran.Location = New System.Drawing.Point(144, 147)
        Me.txt_pengeluaran.MaxLength = 9
        Me.txt_pengeluaran.Name = "txt_pengeluaran"
        Me.txt_pengeluaran.Size = New System.Drawing.Size(179, 22)
        Me.txt_pengeluaran.TabIndex = 7
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 206)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(113, 16)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Simpanan Lancar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(13, 178)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 16)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Lama Usaha"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(13, 150)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 16)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Pengeluaran"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(13, 122)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Penghasilan"
        '
        'txt_penghasilan
        '
        Me.txt_penghasilan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_penghasilan.Location = New System.Drawing.Point(144, 119)
        Me.txt_penghasilan.MaxLength = 9
        Me.txt_penghasilan.Name = "txt_penghasilan"
        Me.txt_penghasilan.Size = New System.Drawing.Size(179, 22)
        Me.txt_penghasilan.TabIndex = 6
        '
        'txt_tanggungan
        '
        Me.txt_tanggungan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_tanggungan.Location = New System.Drawing.Point(144, 91)
        Me.txt_tanggungan.MaxLength = 2
        Me.txt_tanggungan.Name = "txt_tanggungan"
        Me.txt_tanggungan.Size = New System.Drawing.Size(241, 22)
        Me.txt_tanggungan.TabIndex = 2
        '
        'txt_no_survey
        '
        Me.txt_no_survey.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_survey.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_survey.Name = "txt_no_survey"
        Me.txt_no_survey.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_survey.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Tanggungan"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tgl. Survey"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. Survey"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(178, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Cetak Survey"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txt_bidang)
        Me.GroupBox2.Controls.Add(Me.asdafasd)
        Me.GroupBox2.Controls.Add(Me.btn_cari_simpanan)
        Me.GroupBox2.Controls.Add(Me.txt_jml)
        Me.GroupBox2.Controls.Add(Me.txt_nama)
        Me.GroupBox2.Controls.Add(Me.txt_no_pinj)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 83)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(435, 154)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Pinjaman"
        '
        'txt_bidang
        '
        Me.txt_bidang.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_bidang.Location = New System.Drawing.Point(144, 119)
        Me.txt_bidang.Name = "txt_bidang"
        Me.txt_bidang.Size = New System.Drawing.Size(241, 22)
        Me.txt_bidang.TabIndex = 4
        '
        'asdafasd
        '
        Me.asdafasd.AutoSize = True
        Me.asdafasd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.asdafasd.Location = New System.Drawing.Point(13, 122)
        Me.asdafasd.Name = "asdafasd"
        Me.asdafasd.Size = New System.Drawing.Size(94, 16)
        Me.asdafasd.TabIndex = 3
        Me.asdafasd.Text = "Bidang Usaha"
        '
        'btn_cari_simpanan
        '
        Me.btn_cari_simpanan.Location = New System.Drawing.Point(329, 35)
        Me.btn_cari_simpanan.Name = "btn_cari_simpanan"
        Me.btn_cari_simpanan.Size = New System.Drawing.Size(75, 23)
        Me.btn_cari_simpanan.TabIndex = 0
        Me.btn_cari_simpanan.Text = "Cari"
        Me.btn_cari_simpanan.UseVisualStyleBackColor = True
        '
        'txt_jml
        '
        Me.txt_jml.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jml.Location = New System.Drawing.Point(144, 91)
        Me.txt_jml.Name = "txt_jml"
        Me.txt_jml.Size = New System.Drawing.Size(241, 22)
        Me.txt_jml.TabIndex = 2
        '
        'txt_nama
        '
        Me.txt_nama.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nama.Location = New System.Drawing.Point(144, 63)
        Me.txt_nama.Name = "txt_nama"
        Me.txt_nama.Size = New System.Drawing.Size(241, 22)
        Me.txt_nama.TabIndex = 1
        '
        'txt_no_pinj
        '
        Me.txt_no_pinj.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_pinj.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_pinj.Name = "txt_no_pinj"
        Me.txt_no_pinj.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_pinj.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(13, 94)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(110, 16)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "Jumlah Pinjaman"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(13, 66)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(108, 16)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Nama Peminjam"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 38)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(88, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "No. Pinjaman"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_kuar)
        Me.GroupBox3.Controls.Add(Me.btn_batal)
        Me.GroupBox3.Controls.Add(Me.btn_simpan)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 590)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(435, 75)
        Me.GroupBox3.TabIndex = 25
        Me.GroupBox3.TabStop = False
        '
        'btn_kuar
        '
        Me.btn_kuar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_kuar.Location = New System.Drawing.Point(310, 26)
        Me.btn_kuar.Name = "btn_kuar"
        Me.btn_kuar.Size = New System.Drawing.Size(75, 32)
        Me.btn_kuar.TabIndex = 15
        Me.btn_kuar.Text = "Keluar"
        Me.btn_kuar.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Location = New System.Drawing.Point(179, 26)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(78, 32)
        Me.btn_batal.TabIndex = 14
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan
        '
        Me.btn_simpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan.Location = New System.Drawing.Point(31, 26)
        Me.btn_simpan.Name = "btn_simpan"
        Me.btn_simpan.Size = New System.Drawing.Size(90, 32)
        Me.btn_simpan.TabIndex = 13
        Me.btn_simpan.Text = "Simpan"
        Me.btn_simpan.UseVisualStyleBackColor = True
        '
        'txt_no_anggota_tmp
        '
        Me.txt_no_anggota_tmp.AutoSize = True
        Me.txt_no_anggota_tmp.ForeColor = System.Drawing.Color.Transparent
        Me.txt_no_anggota_tmp.Location = New System.Drawing.Point(397, 41)
        Me.txt_no_anggota_tmp.Name = "txt_no_anggota_tmp"
        Me.txt_no_anggota_tmp.Size = New System.Drawing.Size(0, 13)
        Me.txt_no_anggota_tmp.TabIndex = 31
        '
        'CetakSurvey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(471, 736)
        Me.Controls.Add(Me.txt_no_anggota_tmp)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CetakSurvey"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cetak Survey"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_simp_lancar As System.Windows.Forms.TextBox
    Friend WithEvents txt_lama As System.Windows.Forms.TextBox
    Friend WithEvents txt_pengeluaran As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_penghasilan As System.Windows.Forms.TextBox
    Friend WithEvents txt_tanggungan As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_survey As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_simp_kel As System.Windows.Forms.TextBox
    Friend WithEvents txt_jaminan As System.Windows.Forms.TextBox
    Friend WithEvents txt_kesimpulan As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_cari_simpanan As System.Windows.Forms.Button
    Friend WithEvents txt_jml As System.Windows.Forms.TextBox
    Friend WithEvents txt_nama As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_pinj As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txt_bidang As System.Windows.Forms.TextBox
    Friend WithEvents asdafasd As System.Windows.Forms.Label
    Friend WithEvents dt_survey As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_kuar As System.Windows.Forms.Button
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents btn_simpan As System.Windows.Forms.Button
    Friend WithEvents txt_no_anggota_tmp As System.Windows.Forms.Label
End Class
