﻿Imports MySql.Data.MySqlClient
Public Class EntriPinjaman
    Dim pinjaman As New ClsPinjaman
    Private Sub EntriPinjaman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Bersih()
    End Sub
    Private Sub SetData()
        pinjaman.no_pinjaman = txt_no_pinjaman.Text
        pinjaman.no_anggota = txt_no_anggota.Text
        pinjaman.tgl_pinjaman = dt_simpanan.Value
        pinjaman.jml_pinjaman = txt_jmlsimpan.Text
        pinjaman.keterangan = txt_ket.Text
        pinjaman.lama_angsur = txt_lama_angsur.Text
    End Sub
    Private Sub Bersih()
        txt_no_anggota.Enabled = False
        txt_nm_anggota.Enabled = False
        txt_no_pinjaman.Enabled = False
        txt_no_anggota.Clear()
        txt_nm_anggota.Clear()
        txt_no_pinjaman.Text = pinjaman.Autonumber
        txt_jmlsimpan.Clear()
        txt_ket.Clear()
        txt_lama_angsur.Clear()
        btn_cari.Focus()
    End Sub
    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub

    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        popupAnggota.ShowDialog()
    End Sub

    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        pinjaman.Simpan()
        Bersih()
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Bersih()
    End Sub
End Class