﻿Imports MySql.Data.MySqlClient
Public Class CetakSurvey

    Dim survey As New ClsSurvey
    Dim anggota As New ClsAnggota

    Private Sub CetakSurvey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Bersih()
    End Sub

    Private Sub Bersih()
        txt_no_survey.Enabled = False
        txt_no_survey.Text = survey.Autonumber
        txt_no_pinj.Enabled = False
        txt_nama.Enabled = False
        txt_jml.Enabled = False
        txt_bidang.Enabled = False
        txt_no_survey.Enabled = False
        dt_survey.Value = Now
        txt_tanggungan.Clear()
        txt_pengeluaran.Clear()
        txt_penghasilan.Clear()
        txt_lama.Clear()
        txt_simp_kel.Clear()
        txt_simp_lancar.Clear()
        txt_jaminan.Clear()
        txt_kesimpulan.Clear()
        btn_cari_simpanan.Focus()
    End Sub

    Private Sub SetData()
        survey.no_survey = txt_no_survey.Text
        survey.tgl_survey = dt_survey.Value
        survey.tanggungan = txt_tanggungan.Text
        survey.penghasilan = txt_penghasilan.Text
        survey.pengeluaran = txt_pengeluaran.Text
        survey.lama_usaha = txt_lama.Text
        survey.simpanan_lancar = txt_simp_lancar.Text
        survey.simpanan_kelompok = txt_simp_kel.Text
        survey.jaminan_lain = txt_jaminan.Text
        survey.kesimpulan = txt_kesimpulan.Text
        survey.no_pinjaman = txt_no_pinj.Text
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari_simpanan.Click
        popupPinjaman.ShowDialog()
    End Sub

    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Bersih()
    End Sub

    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        survey.Simpan()
        Bersih()
        CtkSurvey(txt_no_survey.Text)
    End Sub

    Private Sub txt_no_pinj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_no_pinj.TextChanged
        anggota.Get_NoAnggota(txt_no_anggota_tmp.Text)
        MessageBox.Show(anggota.nm_anggota)
        txt_nama.Text = anggota.nm_anggota
    End Sub
End Class