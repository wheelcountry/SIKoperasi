﻿Imports MySql.Data.MySqlClient
Public Class EntriPengunduranDiri
    Dim pengunduran As New ClsPengunduran

    Private Sub EntriPengunduranDiri_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Dispose()
        End If
    End Sub
    Private Sub EntriPengunduran_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bersih()
    End Sub

    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        pengunduran.Simpan()
        bersih()
    End Sub
    Private Sub bersih()
        txt_no_anggota.Enabled = False
        txt_no_pengunduran.Enabled = False
        txt_nm_anggota.Enabled = False
        txt_no_pengunduran.Text = pengunduran.Autonumber
        txt_no_anggota.Clear()
        txt_nm_anggota.Clear()
        txt_alasan.Clear()
        dt_pengunduran.Value = Now
    End Sub
    Private Sub SetData()
        pengunduran.no_pengunduran = txt_no_pengunduran.Text
        pengunduran.tgl_pengunduran = dt_pengunduran.Value
        pengunduran.no_anggota = txt_no_anggota.Text
        pengunduran.alasan = txt_alasan.Text
    End Sub

    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        popupAnggota.ShowDialog()
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        bersih()
    End Sub

    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub
End Class