﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EntriDataSimpanan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txt_no_anggota = New System.Windows.Forms.TextBox
        Me.txt_nm_anggota = New System.Windows.Forms.TextBox
        Me.btn_cari = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txt_saldolama = New System.Windows.Forms.TextBox
        Me.txt_jmltarik = New System.Windows.Forms.TextBox
        Me.dt_simpanan = New System.Windows.Forms.DateTimePicker
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.cmb_jenis_simpanan = New System.Windows.Forms.ComboBox
        Me.txt_jmlsimpan = New System.Windows.Forms.TextBox
        Me.txt_no_simpanan = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btn_tarik = New System.Windows.Forms.Button
        Me.btn_kuar = New System.Windows.Forms.Button
        Me.btn_batal = New System.Windows.Forms.Button
        Me.btn_simpan = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(268, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Form Entri Simpanan"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. Anggota"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Nama"
        '
        'txt_no_anggota
        '
        Me.txt_no_anggota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_anggota.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_anggota.Name = "txt_no_anggota"
        Me.txt_no_anggota.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_anggota.TabIndex = 0
        '
        'txt_nm_anggota
        '
        Me.txt_nm_anggota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nm_anggota.Location = New System.Drawing.Point(144, 63)
        Me.txt_nm_anggota.Name = "txt_nm_anggota"
        Me.txt_nm_anggota.Size = New System.Drawing.Size(241, 22)
        Me.txt_nm_anggota.TabIndex = 0
        '
        'btn_cari
        '
        Me.btn_cari.Location = New System.Drawing.Point(329, 35)
        Me.btn_cari.Name = "btn_cari"
        Me.btn_cari.Size = New System.Drawing.Size(75, 23)
        Me.btn_cari.TabIndex = 0
        Me.btn_cari.Text = "Cari"
        Me.btn_cari.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_cari)
        Me.GroupBox1.Controls.Add(Me.txt_nm_anggota)
        Me.GroupBox1.Controls.Add(Me.txt_no_anggota)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 88)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(435, 102)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Anggota"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txt_saldolama)
        Me.GroupBox2.Controls.Add(Me.txt_jmltarik)
        Me.GroupBox2.Controls.Add(Me.dt_simpanan)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.cmb_jenis_simpanan)
        Me.GroupBox2.Controls.Add(Me.txt_jmlsimpan)
        Me.GroupBox2.Controls.Add(Me.txt_no_simpanan)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 196)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(435, 219)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Simpanan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 181)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 16)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Saldo Lama"
        '
        'txt_saldolama
        '
        Me.txt_saldolama.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_saldolama.Location = New System.Drawing.Point(144, 178)
        Me.txt_saldolama.MaxLength = 9
        Me.txt_saldolama.Name = "txt_saldolama"
        Me.txt_saldolama.ReadOnly = True
        Me.txt_saldolama.Size = New System.Drawing.Size(179, 22)
        Me.txt_saldolama.TabIndex = 6
        '
        'txt_jmltarik
        '
        Me.txt_jmltarik.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jmltarik.Location = New System.Drawing.Point(144, 150)
        Me.txt_jmltarik.MaxLength = 9
        Me.txt_jmltarik.Name = "txt_jmltarik"
        Me.txt_jmltarik.Size = New System.Drawing.Size(179, 22)
        Me.txt_jmltarik.TabIndex = 5
        '
        'dt_simpanan
        '
        Me.dt_simpanan.CustomFormat = "dd/MM/yyyy"
        Me.dt_simpanan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_simpanan.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_simpanan.Location = New System.Drawing.Point(144, 64)
        Me.dt_simpanan.Name = "dt_simpanan"
        Me.dt_simpanan.Size = New System.Drawing.Size(179, 22)
        Me.dt_simpanan.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(13, 153)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Jumlah Tarik"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(115, 16)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Jumlah Simpanan"
        '
        'cmb_jenis_simpanan
        '
        Me.cmb_jenis_simpanan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_jenis_simpanan.FormattingEnabled = True
        Me.cmb_jenis_simpanan.Items.AddRange(New Object() {"Pokok", "Wajib", "Sukarela"})
        Me.cmb_jenis_simpanan.Location = New System.Drawing.Point(144, 92)
        Me.cmb_jenis_simpanan.Name = "cmb_jenis_simpanan"
        Me.cmb_jenis_simpanan.Size = New System.Drawing.Size(179, 24)
        Me.cmb_jenis_simpanan.TabIndex = 3
        '
        'txt_jmlsimpan
        '
        Me.txt_jmlsimpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jmlsimpan.Location = New System.Drawing.Point(144, 122)
        Me.txt_jmlsimpan.MaxLength = 9
        Me.txt_jmlsimpan.Name = "txt_jmlsimpan"
        Me.txt_jmlsimpan.Size = New System.Drawing.Size(179, 22)
        Me.txt_jmlsimpan.TabIndex = 4
        '
        'txt_no_simpanan
        '
        Me.txt_no_simpanan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_simpanan.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_simpanan.Name = "txt_no_simpanan"
        Me.txt_no_simpanan.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_simpanan.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 95)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Jenis Simpanan"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(13, 69)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(123, 16)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Tanggal Simpanan"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(13, 38)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "No. Simpanan"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_tarik)
        Me.GroupBox3.Controls.Add(Me.btn_kuar)
        Me.GroupBox3.Controls.Add(Me.btn_batal)
        Me.GroupBox3.Controls.Add(Me.btn_simpan)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 421)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(435, 75)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        '
        'btn_tarik
        '
        Me.btn_tarik.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tarik.Location = New System.Drawing.Point(128, 26)
        Me.btn_tarik.Name = "btn_tarik"
        Me.btn_tarik.Size = New System.Drawing.Size(84, 32)
        Me.btn_tarik.TabIndex = 8
        Me.btn_tarik.Text = "Tarik"
        Me.btn_tarik.UseVisualStyleBackColor = True
        '
        'btn_kuar
        '
        Me.btn_kuar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_kuar.Location = New System.Drawing.Point(329, 26)
        Me.btn_kuar.Name = "btn_kuar"
        Me.btn_kuar.Size = New System.Drawing.Size(75, 32)
        Me.btn_kuar.TabIndex = 11
        Me.btn_kuar.Text = "Keluar"
        Me.btn_kuar.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Location = New System.Drawing.Point(231, 26)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(78, 32)
        Me.btn_batal.TabIndex = 10
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan
        '
        Me.btn_simpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan.Location = New System.Drawing.Point(16, 26)
        Me.btn_simpan.Name = "btn_simpan"
        Me.btn_simpan.Size = New System.Drawing.Size(90, 32)
        Me.btn_simpan.TabIndex = 7
        Me.btn_simpan.Text = "Simpan"
        Me.btn_simpan.UseVisualStyleBackColor = True
        '
        'EntriDataSimpanan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(464, 502)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "EntriDataSimpanan"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entri Simpanan"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_no_anggota As System.Windows.Forms.TextBox
    Friend WithEvents txt_nm_anggota As System.Windows.Forms.TextBox
    Friend WithEvents btn_cari As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmb_jenis_simpanan As System.Windows.Forms.ComboBox
    Friend WithEvents txt_jmlsimpan As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_simpanan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_saldolama As System.Windows.Forms.TextBox
    Friend WithEvents txt_jmltarik As System.Windows.Forms.TextBox
    Friend WithEvents dt_simpanan As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_kuar As System.Windows.Forms.Button
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents btn_simpan As System.Windows.Forms.Button
    Friend WithEvents btn_tarik As System.Windows.Forms.Button
End Class
