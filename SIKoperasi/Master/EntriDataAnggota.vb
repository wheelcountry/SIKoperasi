﻿Imports MySql.Data.MySqlClient
Public Class EntriDataAnggota
    Dim anggota As New ClsAnggota

    Private Sub EntriDataAnggota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bersih()
    End Sub
    Private Sub bersih()
        txt_no_anggota.Enabled = False
        txt_no_anggota.Text = anggota.Autonumber
        txt_almt_anggota.Clear()
        txt_jml_keluarga.Clear()
        txt_nm_anggota.Clear()
        txt_pendidikan.Clear()
        txt_pekerjaan.Clear()
        txt_tlp.Clear()
        cmb_agama.Text = ""
        btn_simpan.Enabled = True
        btn_hapus.Enabled = False
        btn_ubah.Enabled = False
        txt_nm_anggota.Focus()
        rd1_pria.Checked = True
        rd2_nikah.Checked = True
    End Sub
    Private Sub SetData()
        anggota.no_anggota = txt_no_anggota.Text
        anggota.nm_anggota = txt_nm_anggota.Text
        anggota.almt_anggota = txt_almt_anggota.Text
        If rd1_pria.Checked = True Then
            anggota.jenkel = 0
        ElseIf rd1_wanita.Checked = True Then
            anggota.jenkel = 1
        Else
            MessageBox.Show("wat")
        End If
        If rd2_nikah.Checked = True Then
            anggota.status_marital = 0
        ElseIf rd2_single.Checked = True Then
            anggota.status_marital = 1
        ElseIf rd2_cerai.Checked = True Then
            anggota.status_marital = 2
        Else
            MessageBox.Show("wat2")
        End If
        anggota.agama = cmb_agama.Text
        anggota.pendidikan = txt_pendidikan.Text
        anggota.pekerjaan = txt_pekerjaan.Text
        anggota.tlp_anggota = txt_tlp.Text
        anggota.jml_keluarga = txt_jml_keluarga.Text
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        bersih()
    End Sub

    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub

    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        anggota.Simpan()
        bersih()
    End Sub

    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        popupAnggota.ShowDialog()
    End Sub

    Private Sub btn_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ubah.Click
        SetData()
        anggota.Ubah()
        bersih()
    End Sub

    Private Sub btn_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus.Click
        SetData()
        anggota.Hapus()
        bersih()
    End Sub
End Class