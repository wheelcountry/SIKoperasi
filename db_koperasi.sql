-- --------------------------------------------------------
-- Host:                         192.168.1.95
-- Server version:               10.1.24-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_koperasi
DROP DATABASE IF EXISTS `db_koperasi`;
CREATE DATABASE IF NOT EXISTS `db_koperasi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `db_koperasi`;

-- Dumping structure for table db_koperasi.anggota
DROP TABLE IF EXISTS `anggota`;
CREATE TABLE IF NOT EXISTS `anggota` (
  `no_anggota` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nm_anggota` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `almt_anggota` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenkel` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_marital` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pendidikan` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pekerjaan` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tlp_anggota` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jml_keluarga` decimal(2,0) DEFAULT NULL,
  PRIMARY KEY (`no_anggota`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.angsuran
DROP TABLE IF EXISTS `angsuran`;
CREATE TABLE IF NOT EXISTS `angsuran` (
  `no_angsuran` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_angsuran` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pembayaran` decimal(9,0) DEFAULT NULL,
  `sisa_pinjaman` decimal(9,0) DEFAULT NULL,
  `angsuran_ke` decimal(9,0) DEFAULT NULL,
  `no_pinjaman` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_angsuran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.pendaftaran
DROP TABLE IF EXISTS `pendaftaran`;
CREATE TABLE IF NOT EXISTS `pendaftaran` (
  `no_pendaftaran` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pendaftaran` datetime DEFAULT NULL,
  `bidang_usaha` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjualan_perbulan` decimal(9,0) DEFAULT NULL,
  `keuntungan_perbulan` decimal(9,0) DEFAULT NULL,
  `besar_modal` decimal(9,0) DEFAULT NULL,
  `tempat_tinggal` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_usaha` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `almt_usaha` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_anggota` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_pendaftaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.pengunduran
DROP TABLE IF EXISTS `pengunduran`;
CREATE TABLE IF NOT EXISTS `pengunduran` (
  `no_pengunduran` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pengunduran` date DEFAULT NULL,
  `alasan` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_anggota` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_pengunduran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.pinjaman
DROP TABLE IF EXISTS `pinjaman`;
CREATE TABLE IF NOT EXISTS `pinjaman` (
  `no_pinjaman` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pinjaman` datetime DEFAULT NULL,
  `jml_pinjaman` decimal(9,0) DEFAULT NULL,
  `keterangan` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lama_angsur` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_anggota` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_pinjaman`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.simpanan
DROP TABLE IF EXISTS `simpanan`;
CREATE TABLE IF NOT EXISTS `simpanan` (
  `no_simpanan` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_simpanan` datetime DEFAULT NULL,
  `jns_simpanan` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jml_simpanan` decimal(9,0) DEFAULT NULL,
  `jml_tarik` decimal(9,0) DEFAULT NULL,
  `saldo_lama` decimal(9,0) DEFAULT NULL,
  `no_anggota` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_simpanan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.sppd
DROP TABLE IF EXISTS `sppd`;
CREATE TABLE IF NOT EXISTS `sppd` (
  `no_sppd` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_sppd` datetime DEFAULT NULL,
  `plafond` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kegunaan` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jangka_waktu` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bagi_hasil` decimal(9,0) DEFAULT NULL,
  `administrasi` decimal(9,0) DEFAULT NULL,
  `total_penyaluran` decimal(9,0) DEFAULT NULL,
  `nilai_penyaluran` decimal(9,0) DEFAULT NULL,
  `keputusan` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_survey` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_sppd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
-- Dumping structure for table db_koperasi.survey
DROP TABLE IF EXISTS `survey`;
CREATE TABLE IF NOT EXISTS `survey` (
  `no_survey` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_survey` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggungan` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penghasilan` decimal(9,0) DEFAULT NULL,
  `pengeluaran` decimal(9,0) DEFAULT NULL,
  `lama_usaha` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `simpanan_lancar` decimal(9,0) DEFAULT NULL,
  `simpanan_kelompok` decimal(9,0) DEFAULT NULL,
  `jaminan_lain` decimal(9,0) DEFAULT NULL,
  `kesimpulan` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_pinjaman` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`no_survey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
